#!/bin/bash

cd cpa

if [ -f 'CPA_CV_"$CLIENT_NAME"_local_HTTPS.xml' ]
then
  rm CPA_CV_"$CLIENT_NAME"_local_HTTPS.xml
fi

printf "Creating cvwus.pub:\t\t\t\t\t"
CVWUS_CERTIFICATE=$(sed 's/.*-----BEGIN CERTIFICATE-----//;s/-----END CERTIFICATE-----.*$//' ../intermediate/certs/cvwus.cert.pem)
openssl rsa -in ../intermediate/private/cvwus.key.pem -pubout -passin pass:password > cvwus.pub 2> /dev/null
printStatus

printf "Creating $CLIENT_NAME.pub:\t\t\t\t\t"
if [ -z "$CLIENT_CERT_FILE" ]
then
  CLIENT_CERTIFICATE=$(sed 's/.*-----BEGIN CERTIFICATE-----//;s/-----END CERTIFICATE-----.*$//' ../intermediate/certs/"$CLIENT_NAME".cert.pem)
  openssl rsa -in ../intermediate/private/"$CLIENT_NAME".key.pem -pubout -passin pass:password > "$CLIENT_NAME".pub  2> /dev/null
else
  CLIENT_CERTIFICATE=$(sed 's/.*-----BEGIN CERTIFICATE-----//;s/-----END CERTIFICATE-----.*$//' $CLIENT_CERT_FILE)
  openssl rsa -in $CLIENT_KEY_FILE -pubout -passin pass:$CLIENT_KEY_PASSWORD > "$CLIENT_NAME".pub 2> /dev/null
fi
printStatus

printf "Creating the CPA:\t\t\t\t\t"
CVWUS_RSA_MODULUS_EXPONENT=$(java -jar ConvertPublicKeyToRSA.jar cvwus.pub)
CLIENT_RSA_MODULUS_EXPONENT=$(java -jar ConvertPublicKeyToRSA.jar "$CLIENT_NAME".pub)

awk -v CVWUS_CERTIFICATE="$CVWUS_CERTIFICATE" \
    -v CLIENT_CERTIFICATE="$CLIENT_CERTIFICATE" \
    -v CVWUS_RSA_MODULUS_EXPONENT="$CVWUS_RSA_MODULUS_EXPONENT" \
    -v CLIENT_RSA_MODULUS_EXPONENT="$CLIENT_RSA_MODULUS_EXPONENT" \
    -v CLIENT_OIN="$CLIENT_OIN" \
    -v CLIENT_NAME="$CLIENT_NAME" \
    -v CLIENT_PROXY="$CLIENT_PROXY" \
    -v CLIENT_ENDPOINT="$CLIENT_ENDPOINT" \
    -v CVWUS_OIN="$CVWUS_OIN" \
    '{
        sub(/CVWUS_CERTIFICATE/, CVWUS_CERTIFICATE);
        sub(/CLIENT_CERTIFICATE/, CLIENT_CERTIFICATE);
        sub(/CVWUS_RSA_MODULUS_EXPONENT/, CVWUS_RSA_MODULUS_EXPONENT);
        sub(/CLIENT_RSA_MODULUS_EXPONENT/, CLIENT_RSA_MODULUS_EXPONENT);
        sub(/CLIENT_OIN/, CLIENT_OIN);
        sub(/CLIENT_NAME/, CLIENT_NAME);
        sub(/CLIENT_PROXY/, CLIENT_PROXY);
        sub(/CLIENT_PROXY/, CLIENT_PROXY);
        sub(/CLIENT_ENDPOINT/, CLIENT_ENDPOINT);
        sub(/CVWUS_OIN/, CVWUS_OIN);
        print;
        }' CPA_CV_CLIENT_local_HTTPS_template.xml > CPA_CV_"$CLIENT_NAME"_local_HTTPS.xml
printStatus

printf "Moving the CPA to the target folder:\t\t\t"
mv CPA_CV_"$CLIENT_NAME"_local_HTTPS.xml "$CPA_PATH"/CPA_CV_"$CLIENT_NAME"_local_HTTPS.xml
printStatus

printf "Removing pub files:\t\t\t\t\t"
rm cvwus.pub "$CLIENT_NAME".pub
printStatus
cd ..
