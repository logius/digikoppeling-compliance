#!/usr/bin/env bash

rm -rf intermediate

mkdir intermediate
cd intermediate

mkdir certs crl csr newcerts private
chmod 774 private
touch index.txt
echo 1000 > serial

cd ..
cp openssl_intermediate.cnf intermediate/openssl.cnf


printf "Create intermediate private key:\t\t\t"
openssl genrsa -passout pass:intpwd \
	-aes256 \
	-out intermediate/private/intermediate.key.pem 4096 2> /dev/null
printStatus

printf "Create CSR for intermediate:\t\t\t\t"
MSYS_NO_PATHCONV=1 openssl req -passin pass:intpwd \
	-config intermediate/openssl.cnf \
	-new -sha256 \
	-key intermediate/private/intermediate.key.pem \
	-out intermediate/csr/intermediate.csr.pem \
	-subj "/C=NL/ST=Zuid-Holland/O=Logius/OU=Stelseldiensten/CN=Logius Intermediate/serialNumber=00000003271679460000" 2> /dev/null
printStatus


printf "Create intermediate certificate:\t\t\t"
openssl ca -passin pass:capwd \
	-config openssl.cnf -extensions v3_intermediate_ca \
	-days 3650 -notext -md sha256 \
	-in intermediate/csr/intermediate.csr.pem \
	-out intermediate/certs/intermediate.cert.pem \
	-batch 2> /dev/null
printStatus

#echo "Display intermediate certificate"
#openssl x509 -noout -text \
#	-in intermediate/certs/intermediate.cert.pem


printf "Verify intermediate certifcate:\t\t\t\t"
openssl verify -CAfile ca/certs/ca.cert.pem \
	intermediate/certs/intermediate.cert.pem 2>&1  1>/dev/null
printStatus


printf "Create CA certificate chain:\t\t\t\t"
cat intermediate/certs/intermediate.cert.pem \
	ca/certs/ca.cert.pem > intermediate/certs/ca-chain.cert.pem
printStatus
