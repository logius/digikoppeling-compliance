#!/usr/bin/env bash

printf "Create private key for OCSP:\t\t\t\t"
openssl genrsa \
	-out intermediate/private/ocsp.key.pem 4096 2> /dev/null
printStatus


printf "Create CSR for OCSP:\t\t\t\t\t"
MSYS_NO_PATHCONV=1 openssl req -passin pass:ocsppwd \
	-config intermediate/openssl.cnf \
	-new -sha256 \
	-key intermediate/private/ocsp.key.pem \
	-out intermediate/csr/ocsp.csr.pem \
	-subj "/C=NL/ST=Zuid-Holland/O=Logius/OU=Stelseldiensten/CN=ocsp-server/serialNumber=00000003271679460000" 2> /dev/null
printStatus

printf "Create OCSP certificate:\t\t\t\t"
openssl ca -passin pass:intpwd \
	-config intermediate/openssl.cnf \
	-extensions ocsp \
	-days 375 -notext -md sha256 \
	-in intermediate/csr/ocsp.csr.pem \
	-out intermediate/certs/ocsp.cert.pem \
	-batch 2> /dev/null
printStatus