#!/usr/bin/env bash

CURRENT=`pwd`
BASENAME=`basename "$CURRENT"`

if [ "$BASENAME" != "scripts" ]; then
    cd generate-certs/scripts
fi

source .bash_colors
source .cvwus_vars


printf "\nCreating root pair\n"
source ./01-create-root-pair.sh

printf "\nCreating intermediate pair\n"
source ./02-create-intermediate-pair.sh

printf "\nCreating OCSP pair\n"
source ./03-create-ocsp-pair.sh

printf "\nCreating user pair for cvwus\n"
declare pass=password name=cvwus subject="/C=NL/ST=Noord-Holland/CN=proxy-cvwus/serialNumber=$CVWUS_OIN" revoked=false
source ./04-create-user-pair.sh

source ./05-copy-to-ocsp-server.sh

rm -rf keystores
mkdir keystores

printf "\nGenerating keystores for cvwus\n"
declare name=cvwus oin=$CVWUS_OIN
source ./06-generate-keystores.sh

source ./07-copy-keystores.sh

if [ -n "$CLIENT_NAME" ]
then
  printf "\nClient name is $CLIENT_NAME\n"
  source ./08-generate-for-external-client.sh
fi
