#!/bin/bash

if [ ! -d $CVWUS_CERTS_PATH ]
then
  mkdir -p $CVWUS_CERTS_PATH
fi

if [ ! -d $CVWUS_CA_PATH ]
then
  mkdir -p $CVWUS_CA_PATH
fi

if [ ! -d $CVWUS_KEYSTORES_PATH ]
then
  mkdir -p $CVWUS_KEYSTORES_PATH
fi

### cvwus
printf "Copying the cvwus-truststore.jks:\t\t\t"
yes | cp -f keystores/cvwus-truststore.jks        $CVWUS_KEYSTORES_PATH/.
printStatus

printf "Copying the cvwus-keystore.jks:\t\t\t\t"
yes | cp -f keystores/cvwus-keystore.jks          $CVWUS_KEYSTORES_PATH/.
printStatus

printf "Copying the cvwus.cert.pem:\t\t\t\t"
yes | cp -f intermediate/certs/cvwus.cert.pem     $CVWUS_CERTS_PATH/cvwus.pem
printStatus

printf "Copying the cvwus.key.pem:\t\t\t\t"
yes | cp -f intermediate/private/cvwus.key.pem    $CVWUS_CERTS_PATH/cvwus.key
printStatus

printf "Copying the ca-chain.cert.pem:\t\t\t\t"
yes | cp -f intermediate/certs/ca-chain.cert.pem  $CVWUS_CA_PATH/ca-chain.pem
printStatus