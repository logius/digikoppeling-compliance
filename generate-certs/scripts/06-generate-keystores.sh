#!/bin/bash

# Expects the following variables:
# - name, the name of the application
# - oin, the oin used to create the certificate


### Generating keystore

printf "Create $name keystore en importeer keypair:\t\t"
openssl pkcs12 -passin pass:password -passout pass:password -export -name $name -in intermediate/certs/$name.cert.pem -inkey intermediate/private/$name.key.pem -out keystores/$name.p12 2> /dev/null
keytool -importkeystore -destkeystore keystores/$name-keystore.jks -srckeystore keystores/$name.p12 -srcstoretype pkcs12 -alias $name -destalias $oin -srcstorepass password -deststorepass password 2> /dev/null
printStatus

printf "Import intermediate cert in $name truststore:\t\t"
keytool -import -keystore keystores/$name-truststore.jks -storepass password -file intermediate/certs/intermediate.cert.pem -alias intermediate -noprompt 2> /dev/null
printStatus

printf "Import CA cert in $name truststore:\t\t\t"
keytool -import -keystore keystores/$name-truststore.jks -storepass password -file ca/certs/ca.cert.pem -alias ca -noprompt 2> /dev/null
printStatus

printf "Import $name cert in $name truststore:\t\t\t"
keytool -import -keystore keystores/$name-truststore.jks -storepass password -file intermediate/certs/$name.cert.pem -alias $oin -noprompt 2> /dev/null
printStatus

rm -rf keystores/*.p12

printf "Completed keystore creation for $name\n"
