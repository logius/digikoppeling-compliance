
if [ ! -d $CLIENT_CERTS_PATH ]
then
  mkdir -p $CLIENT_CERTS_PATH
fi

if [ ! -d $CLIENT_CA_PATH ]
then
  mkdir -p $CLIENT_CA_PATH
fi

if [ ! -d $CLIENT_KEYSTORES_PATH ]
then
  mkdir -p $CLIENT_KEYSTORES_PATH
fi

if [ -z "$CLIENT_CERT_FILE" ]
then
  declare pass=password \
          name=$CLIENT_NAME \
          subject="/C=NL/ST=Noord-Holland/CN=$CLIENT_PROXY/serialNumber=$CLIENT_OIN" \
          revoked=false \
          oin=$CLIENT_OIN

  printf "Creating user pair for $CLIENT_NAME\n"
  source ./04-create-user-pair.sh

  printf "Generating keystores for $CLIENT_NAME\n"
  source ./06-generate-keystores.sh

  printf "Import CVWUS cert into $CLIENT_NAME-truststore.jks:\t\t"
  keytool -import -keystore keystores/"$CLIENT_NAME"-truststore.jks -storepass password -file intermediate/certs/cvwus.cert.pem -alias $CVWUS_OIN -noprompt 2> /dev/null
  printStatus

  printf "Import $CLIENT_NAME.cert.pem cert into CVWUS truststore:\t"
  keytool -import -keystore keystores/cvwus-truststore.jks -storepass password -file intermediate/certs/"$CLIENT_NAME".cert.pem -alias $CLIENT_OIN -noprompt 2> /dev/null
  printStatus

  ### CLIENT
  yes | cp -f keystores/"$CLIENT_NAME"-truststore.jks                   $CLIENT_KEYSTORES_PATH/.
  yes | cp -f keystores/"$CLIENT_NAME"-keystore.jks                     $CLIENT_KEYSTORES_PATH/.
  yes | cp -f intermediate/certs/"$CLIENT_NAME".cert.pem                $CLIENT_CERTS_PATH/"$CLIENT_NAME".pem
  yes | cp -f intermediate/private/"$CLIENT_NAME".key.pem               $CLIENT_CERTS_PATH/"$CLIENT_NAME".key
else
  printf "Client certificate provided: $(CLIENT_CERT_FILE)"
  printf "Client key provided: $(CLIENT_KEY_FILE)"
  printf "Client truststore provided: $(CLIENT_CERT_FILE)"
  printf "Client keystore provided: $(CLIENT_KEYSTORE_FILE)"

  printf "Import CVWUS cert into $(CLIENT_TRUSTSTORE_FILE):\t\t\t"
  keytool -import -keystore $CLIENT_TRUSTSTORE_FILE -storepass $CLIENT_TRUSTSTORE_PASSWORD -file intermediate/certs/cvwus.cert.pem -alias $CVWUS_OIN -noprompt 2> /dev/null
  printStatus

  printf "Import $(CLIENT_CERT_FILE) cert into CVWUS truststore:\t"
  keytool -import -keystore keystores/cvwus-truststore.jks -storepass password -file $CLIENT_CERT_FILE -alias $CLIENT_OIN -noprompt 2> /dev/null
  printStatus

#  yes | cp -f $CLIENT_CERT_FILE               $CLIENT_CERTS_PATH/.
#  yes | cp -f $CLIENT_KEY_FILE                $CLIENT_CERTS_PATH/.
fi

yes | cp -f keystores/cvwus-truststore.jks          $CVWUS_KEYSTORES_PATH/.
yes | cp -f intermediate/certs/ca-chain.cert.pem    $CLIENT_CA_PATH/ca-chain.pem


printf "\nGenerate the CPA\n"
source ./08-01-generate-cpa.sh
