#!/bin/bash

rm -rf $OCSP_SERVER_CONFIG_PATH
mkdir -p $OCSP_SERVER_CONFIG_PATH

printf "Copying the OCSP content:\t\t\t\t"
yes | cp -f intermediate/index.txt intermediate/certs/ca-chain.cert.pem intermediate/certs/intermediate.cert.pem intermediate/private/ocsp.key.pem intermediate/certs/ocsp.cert.pem $OCSP_SERVER_CONFIG_PATH
printStatus