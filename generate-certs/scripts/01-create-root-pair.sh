#!/usr/bin/env bash

rm -rf ca

mkdir ca
cd ca

mkdir certs newcerts private
chmod 774 private
touch index.txt
echo 1000 > serial

cd ..
cp openssl.cnf ca/openssl.cnf

printf "Create root private key:\t\t\t\t"
openssl genrsa -passout pass:capwd -aes256 -out ca/private/ca.key.pem 4096  2> /dev/null
printStatus

printf "Create root certificate:\t\t\t\t"
MSYS_NO_PATHCONV=1 openssl req -passin pass:capwd \
	-config openssl.cnf \
	-key ca/private/ca.key.pem \
	-new -x509 -days 730 -sha256 \
	-extensions v3_ca \
	-out ca/certs/ca.cert.pem \
	-subj "/C=NL/ST=Zuid-Holland/O=Logius/OU=Stelseldiensten/CN=Logius/serialNumber=00000003271679460000" 2> /dev/null
printStatus

#echo "Display root certificate"
#openssl x509 -noout -text -in ca/certs/ca.cert.pem


