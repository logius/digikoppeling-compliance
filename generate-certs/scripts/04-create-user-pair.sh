#!/usr/bin/env bash

# Expects the following variables:
# - pass, the password for the generated private key
# - name, the name of the user
# - subject, the subject DN for the generated certificate
# - revoked, whether the generated certificate should be revoked


printf "Create private key:\t\t\t\t\t"
openssl genrsa -passout pass:$pass \
	-aes256 \
	-out intermediate/private/$name.key.pem 2048 2> /dev/null
printStatus
chmod 774 intermediate/private/$name.key.pem


printf "Create CSR:\t\t\t\t\t\t"
MSYS_NO_PATHCONV=1 openssl req -passin pass:$pass \
	-config intermediate/openssl.cnf \
	-new -sha256 \
	-key intermediate/private/$name.key.pem \
	-out intermediate/csr/$name.csr.pem \
	-subj $subject 2> /dev/null
printStatus

printf "Create certificate for $name:\t\t\t\t"
openssl ca -passin pass:intpwd \
	-config intermediate/openssl.cnf \
	-extensions usr_cert \
	-days 375 -notext -md sha256 \
	-in intermediate/csr/$name.csr.pem \
	-out intermediate/certs/$name.cert.pem \
	-batch 2> /dev/null
printStatus

chmod 774 intermediate/certs/$name.cert.pem


printf "Verify certificate:\t\t\t\t\t"
openssl verify -CAfile intermediate/certs/ca-chain.cert.pem \
      intermediate/certs/$name.cert.pem 2>&1 1>/dev/null
printStatus


#echo "Display certificate"
#openssl x509 -noout -text \
#	-in intermediate/certs/$name.cert.pem


if [ "$revoked" = "true" ]; then
	printf "Revoke certificate:\t\t\t\t\t"
	openssl ca -passin pass:intpwd \
		-config intermediate/openssl.cnf \
		-revoke intermediate/certs/$name.cert.pem 2> /dev/null
  printStatus
fi
