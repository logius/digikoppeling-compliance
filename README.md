# CVWUS (Compliancevoorziening)

## Wat is de compliancevoorziening?
De compliancevoorziening (CVWUS) is een portaal waarmee u kunt testen of uw software conform de Digikoppeling standaard is ontwikkeld en geïmplementeerd. Voorheen enkel te raadplegen via www.portaal.digikoppeling.nl, is het nu mogelijk om via deze docker images lokaal uw eigen digikoppeling compliancetesten op te zetten.

De functionaliteiten die hierbij worden aangeboden zijn:
- Uploaden en valideren van certificaten om te kijken of deze geschikt zijn om berichten conform Digikoppeling uit te wisselen;
- Testen van het berichtenverkeer voor alle WUS en ebMS2 profielen;
- In een rapportage per regel van de koppelvlakstandaard WUS of ebMS2 zien of de geteste service compliant is aan de koppelvlakstandaard. <b>Belangrijk</b>: Nog niet voor alle regels zijn validaties geïmplementeerd;
- Beheren van instellingen, zoals het definiëren van te testen endpoints, certificaten en CPAs.


## Architectuur


![CVWUS Minimal Architecture](Architecture%20diagram-Page-2.png)

Zoals te zien is in het diagram hierboven gebruiken de docker containers ieder hun eigen external volume.

* CV: Voor key/truststore en een directory met benodigde (CA) certificaten
* OCSP: Voor een lijst met valide/ingetrokken certificaten en een eigen certificaat/key
* Postgres: Om data op te slaan op de host, zodat deze niet verloren gaat bij het afsluiten van de docker container.

Zoals getoond in het bovenstaande diagram::

- Een custom docker netwerk wordt gebruikt. Dit verzorgt netwerkisolatie van mogelijk andere op de host draaiende containers. Het subnet dat hiervoor gekozen wordt is aan te passen in de configuratie om bijvoorbeeld mogelijke conflicten met bestaande netwerken te vermijden.

### OCSP
De compliancevoorziening communiceert met de OCSP server om self-signed certificaten te valideren. De certificaten die worden gevalideerd zijn die van de compliancevoorziening zelf (te genereren via scripts beschreven in sectie 'Genereren Certificaten/Keystores/CPA') en een eventueel client certificaat dat gegenereerd wordt met deze scripts.

### Database (Postgresql)
Een image voor de database is onderdeel van deze compliancevoorziening. Deze is voorgeladen met het schema dat benodigd is voor de database van de compliancevoorziening zelf en die van de ebMS adapter die wordt gebruikt.
Het is mogelijk (en aan te raden) om ook uw eigen (adapter)data te mappen naar de host machine via een volume (zoals is te zien in de voorbeeld docker-compose file). Indien geen mapping wordt aangemaakt dan wordt data lokaal in de container opgeslagen en zullen alle configuraties en testresultaten verloren gaan bij het verwijderen van de container.

### Compliancevoorziening (CV)
Deze image bevat 3 verschillende webapplicaties (wars):
  - CV (De compliancevoorziening)
  - ebMS adapter (Biedt onder meer endpoints aan voor het versturen/verwerken van ebMS verkeer)
  - ebMS admin console (Biedt inzicht in (de status van) het verkeer dat is verstuurd en ontvangen)
  
### Dart (optioneel)
Deze image is een voorbeeld implementatie van een WUS client die kan worden gebruikt om WUS interacties met de Compliancevoorziening uit te testen. Hiermee kan o.a 
een goede installatie en configuratie van de CV container worden gevalideerd. Deze component wordt gestart bij gebruik van de 'docker-compose-with-dart.yml' 
i.p.v. de standaard 'docker-compose.yml'    

## Hoe start ik de docker containers?

U dient de volgende bestanden te hebben (in dezelfde folder):

- docker-compose.yml
- docker-compose-with-dart.yml
- .env
- generate-certs (folder)

Controleer voordat de volgende commando's worden uitgevoerd eerst de .env file en pas variabelen waarnodig aan. Hieronder leest u welke variabele waarvoor wordt gebruikt.

De volgende commando's dienen te worden uitgevoerd:

- Voor het genereren van certificaten/keystores/CPA: ```generate-certs/scripts/create-all.sh```
- Voor het opstarten van de docker containers: ```docker-compose up -d```


### .env

| Variabelenaam | Toelichting | Default waarde | 
| ------------- | ----------- | -------------- |
| DOCKER_REGISTRY | Docker registry waar de images van worden gepulled. | logius-st-docker.ace.nl.capgemini.com |
| DOCKER_GATEWAY_HOST | IP van de HOST machine, waarvan de docker-compose wordt gestart. Wordt gebruik voor het opzetten van een [custom netwerk](https://docs.docker.com/engine/reference/commandline/network_create/). | 172.18.0.1 |
| OCSP_CONFIG_PATH | Folder met de (gegenereerde) configuratie voor de OCSP server. Gebruikt voor de gegenereerde self-signed certificaten. | ./generate-certs/generated/ocsp-config |
| DATABASE_PERSISTENT_DATA_PATH | Folder waar database bestanden worden opgeslagen zodat data bewaard blijft na het verwijderen van de container. | /tmp/postgres-data |
| CV_HTTP_PORT | HTTP poort voor het bereiken van [CV](http://localhost:8800/CV) | 8800 |
| CV_HTTPS_PORT | HTTPS poort voor het bereiken van [CV](https://localhost:8443/CV) | 8443 |
| CVWUS_KEYSTORES_PATH | Folder met de (gegenereerde) key/truststore voor Compliancevoorziening. De truststore bevat enkel de issuer van de self-signed certificaten die zijn gegenereerd. | ./generate-certs/generated/cvwus-keystores |
| CVWUS_CERTS_PATH | Folder met het publieke certificaat en sleutel van de Compliancevoorziening. Hier wordt onder meer naar gerefereerd in de proxy-configuratie. | ./generate-certs/generated/cvwus-certs |
| CVWUS_CA_PATH | Folder met vertrouwde CAs door Compliancevoorziening. | ./generate-certs/generated/cvwus-ca |
| CVWUS_LOGS_PATH | Folder met de gegenereerde logs door Compliancevoorziening. | ./generate-certs/logs/cvwus |

### Genereren Certificaten/Keystores/CPA

De Compliancevoorziening maakt gebruik van certificaten en CPAs voor het berichtenverkeer. Voor deze opzet zal de Compliancevoorziening zelf zijn eigen self-signed certificaat gebruiken. 
Om zaken makkelijker te maken worden enkele scripts meegeleverd (de folder generate-certs) die dit eenvoudig maakt.

#### Structuur van generate-certs

| Foldernaam | Toelichting |
| ---------- | ----------- |
| scripts | Een lijst van scripts die wordt gebruikt voor het genereren van de self-signed certificaten voor de Compliancevoorziening (en eventueel een 'Client' met eigen ebms endpoints). |
| generated | De bestanden die woorden gegenereerd door de bovengenoemde scripts. Naar deze bestanden wordt gerefereerd in de docker-compose.yml/.env en kunnen verder worden uitgebreid. |
| logs | De log files van de verschillende docker containers die meer informatie kunnen verschaffen |

#### Hoe te gebruiken?
Er zijn twee verschillende scenario's mogelijk.
 
##### Scenario 1: Genereer (self signed) certificaten voor zowel de Compliancevoorziening als client

De volgende zaken worden gegenereerd:
 - Een (self signed) certificaat voor de Compliancevoorziening
 - Een (self signed) certificaat voor uw client applicatie (zelf te installeren)
 - Een keystore voor de Compliancevoorziening met het eigen certificaat
 - Een truststore voor de Compliancevoorziening met het certificaat van uw client
 - Een keystore voor uw client met het eigen certificaat (zelf te installeren)
 - Een truststore voor uw client met het certificaat van de Compliancevoorziening (zelf te installeren)
 - Een CPA voor communicatie tussen de Compliancevoorziening en uw client
  
De volgende variabelen moeten eerst worden ingesteld in generated_certs/scripts/.cvwus_vars:

| Variabelenaam | Default waarde | Toelichting |
| ---------- | ----------- | ----------- |
| CVWUS_KEYSTORES_PATH | ./../generated/cvwus-keystores (locatie van generatie) | De locatie van de key/truststore van de Compliancevoorziening |
| CVWUS_CERTS_PATH | ./../generated/cvwus-certs | De locatie van het (publieke) certificaat van de Compliance voorziening |
| CVWUS_CA_PATH | ./../generated/cvwus-ca | De locatie van het certificaat van de gebruikte Certificate Authority |
| CVWUS_OIN | 00000004003214345001 | Het OIN van de Compliancevoorziening |
| OCSP_SERVER_CONFIG_PATH | ./../generated/ocsp-config | De locatie van de gegenereerde OCSP (voor de self signed certificaten) configuratie |
| CPA_PATH | ./../../generated | De locatie van het gegenereerde CPA |
| CLIENT_NAME | Leeg | Een naam van de client (geen speciale karakters/spaties) |
| CLIENT_OIN | Leeg | Het OIN van de client. |
| CLIENT_PROXY | Leeg | De domeinnaam van/proxy voor de client |
| CLIENT_ENDPOINT | Het ebms endpoint van de client. | 
| CLIENT_KEYSTORES_PATH | ./../generated/cvwus-keystores (locatie van generatie) | De locatie van de key/truststore van de client |
| CLIENT_CERTS_PATH | ./../generated/cvwus-certs | De locatie van het (publieke) certificaat van de client |
| CLIENT_CA_PATH | ./../generated/cvwus-ca | De locatie van het certificaat van de gebruikte Certificate Authority |

Voer het volgende commando uit: ```generate-certs/scripts/create-all.sh```.

##### Scenario 2: Genereer (self signed) certificaten voor zowel de Compliancevoorziening, gebruik bestaande certificaten voor de client

De volgende zaken worden gegenereerd:
 - Een (self signed) certificaat voor de Compliancevoorziening
 - Een keystore voor de Compliancevoorziening met het eigen certificaat
 - Een truststore voor de Compliancevoorziening met het certificaat van uw client
 - Het certificaat van de Compliancevoorziening wordt toegevoegd aan de truststore van uw client (gevonden via CLIENT_KEYSTORE_FILE)
 - Een CPA voor communicatie tussen de Compliancevoorziening en uw client

De volgende variabelen moeten eerst worden ingesteld in generated_certs/scripts/.cvwus_vars:

| Variabelenaam | Default waarde | Toelichting |
| ---------- | ----------- | ----------- |
| CVWUS_KEYSTORES_PATH | ./../generated/cvwus-keystores (locatie van generatie) | De locatie van de key/truststore van de Compliancevoorziening |
| CVWUS_CERTS_PATH | ./../generated/cvwus-certs | De locatie van het (publieke) certificaat van de Compliance voorziening |
| CVWUS_CA_PATH | ./../generated/cvwus-ca | De locatie van het certificaat van de gebruikte Certificate Authority |
| CVWUS_OIN | 00000004003214345001 | Het OIN van de Compliancevoorziening |
| OCSP_SERVER_CONFIG_PATH | ./../generated/ocsp-config | De locatie van de gegenereerde OCSP (voor de self signed certificaten) configuratie |
| CPA_PATH | ./../../generated | De locatie van het gegenereerde CPA |
| CLIENT_NAME | Leeg | Een naam van de client (geen speciale karakters/spaties) |
| CLIENT_OIN | Leeg | Het OIN van de client. |
| CLIENT_PROXY | Leeg | De domeinnaam van/proxy voor de client |
| CLIENT_ENDPOINT | Het ebms endpoint van de client. | 
| CLIENT_CERT_FILE | Leeg | De locatie van het certificaat van de client |
| CLIENT_KEY_FILE | Leeg | De locatie van de key van de client |
| CLIENT_KEY_PASSWORD | Leeg | Het wachtwoord van de key van de client |
| CLIENT_TRUSTSTORE_FILE | Leeg | De locatie van de truststore van de client |
| CLIENT_KEYSTORE_FILE | Leeg | De locatie van de keystore van de client |
| CLIENT_TRUSTSTORE_PASSWORD | password | Het wachtwoord voor de gegenereerde truststore |

Voer het volgende commando uit: ```generate-certs/scripts/create-all.sh```.


### Opstarten docker containers (via docker-compose)

Voorbeeld docker-compose.yml:

Variabelen die worden gebruikt via ${} notatie worden uit de .env file gelezen.

```
version: "3"
services:
  db:
    image: ${DOCKER_REGISTRY}/cvwus-postgres:latest
    container_name: cvwus-postgres
    networks:
      - development
    environment:
      POSTGRES_PASSWORD: "postgres"
      POSTGRES_USER: "postgres"
      POSTGRES_MULTIPLE_DATABASES: ocvwus,ebms
      POSTGRES_HOST_AUTH_METHOD: "md5"
    volumes:
      - ${DATABASE_PERSISTENT_DATA_PATH:-db-data}:/var/lib/postgresql/data
    ports:
      - "${DATABASE_PORT}:5432"
  ocsp:
    image: ${DOCKER_REGISTRY}/ocsp-server:latest
    container_name: ocsp-server
    networks:
      - development
    volumes:
      - ${OCSP_CONFIG_PATH}:/config
    ports:
      - "${OCSP_SERVER_PORT}:80"
  cv:
    image: ${DOCKER_REGISTRY}/cvwus:${CVWUS_VERSION}
    depends_on:
      - db
    volumes:
      - ${CVWUS_KEYSTORES_PATH}:/opt/cvwus/keystores
      - ${CVWUS_CERTS_PATH}:/etc/pki/tls/certs
      - ${CVWUS_CA_PATH}:/opt/cvwus/ca
    networks:
      - development
    container_name: cvwus
    extra_hosts:
      proxy-cvwus: ${DOCKER_GATEWAY_HOST:-host.docker.internal}
    environment:
      CV_HTTP_PORT: ${CV_HTTP_PORT}
      CV_HTTPS_PORT: ${CV_HTTPS_PORT}
      CVWUS_DATABASE_HOST: ${DATABASE_HOST:-cvwus-postgres}:${EXTERNAL_DATABASE_PORT:-5432}
    ports:
      - "${CV_HTTP_PORT}:80"
      - "${CV_HTTPS_PORT}:443"
volumes:
  db-data:
    driver: local
networks:
  development:
    ipam:
      driver: default
      config:
        - subnet: ${DOCKER_GATEWAY_HOST}/24
```
Voer het volgende commando uit ``docker-compose up -d`` en wacht tot alle containers zijn opgestart.

Voor het stoppen/verwijderen van de images en containers: ``docker-compose down --rmi 'all'`` zonder de flag --rmi worden de images niet verwijderd.


### Opstarten docker containers (direct, zonder docker-compose)

De environment variabelen die worden genoemd in de onderstaande commando's staan in de meegeleverde .env file. Voordat de onderstaande commando's worden uitgevoerd moet daarom het volgende source commando worden uitgevoerd om deze variabelen actief te maken:

```
source .env
```

(Certificaten/CA/OCSP kunnen worden gegenereerd via script in sectie 'Genereren Certificaten/Keystores/CPA')

Voor het opzetten van het docker network:
```
docker network create --driver=bridge --subnet=$DOCKER_GATEWAY_HOST/24 --gateway=$DOCKER_GATEWAY_HOST development
```

Voor het opzetten van de OCSP server:
```
docker run -itd --name ocsp-server -v $OCSP_CONFIG_PATH:/config logius-st-docker.ace.nl.capgemini.com/ocsp-server:latest
```

Voor het opzetten van de database:
```
docker run -itd -p 5445:5432 --name postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_USER=postgres -e POSTGRES_MULTIPLE_DATABASES=ocvwus,ebms -e POSTGRES_HOST_AUTH_METHOD=md5 -v $DATABASE_PERSISTENT_DATA_PATH:/var/lib/postgresql/data logius-st-docker.ace.nl.capgemini.com/cvwus-postgres:latest
```

Voor het opzetten van de compliancevoorziening:
```
docker run -itd -p 8800:80  -p 443:443 --name cvwus --hostname cvwus --net development -e CV_HTTP_PORT=8800 -e CV_HTTPS_PORT=443 -e CVWUS_DATABASE_HOST=$DOCKER_GATEWAY_HOST:5432 --add-host=proxy-cvwus:$DOCKER_GATEWAY_HOST --add-host=proxy-dart:$DOCKER_GATEWAY_HOST -v $CVWUS_KEYSTORES_PATH:/opt/cvwus/keystores -v $CVWUS_CERTS_PATH:/etc/pki/tls/certs -v $CVWUS_CA_PATH:/opt/cvwus/ca -v $CVWUS_LOG_PATH:/var/log/cvwus logius-st-docker.ace.nl.capgemini.com/cvwus:1.0.0-SNAPSHOT
```

## Benaderen van de applicatie

Na het opstarten van de containers kan je deze benaderen op de volgende wijze:

De compliancevoorziening is te benaderen via https://localhost:8443/CV (of een andere HTTPS poort, zoals is meegegeven bij het opstarten van de container). Het is hier mogelijk om (voor [meer info](Link naar functionele documentatie)):
* WUS/ebMs tests uit te voeren;
* Certificaten/endpoints/CPAs te uploaden, deze worden gebruikt voor WUS en ebMS tests;
* Rapportages van tests inzien

De [ebMS admin console](https://github.com/java-ebms-adapter/ebms-admin-console) is te benaderen via https://localhost:443/ebms-admin. 
* Deze applicatie biedt inzicht in het ebMS verkeer van en naar de compliancevoorziening;
* Maakt gebruik van de ebms-adapter van de compliancevoorziening
* Maakt gebruik van dezelfde database als de compliancevoorziening
* Biedt tevens de mogelijkheid om ebMS berichten te versturen
